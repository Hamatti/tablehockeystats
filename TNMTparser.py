# -*- coding: utf-8 -*-

import codecs
import player, game

def file_len(fname):
	""" Return length of file """
	with open(fname) as f:
		for i, l in enumerate(f):
			pass
	return i + 1

def getPlayer(players, name):
	"""Return first item in sequence where f(item) == True."""
	for player in players:
		if player.name == name: 
			return player

class TNMTparser(object):

	def __init__(self):
		self.players = []
		self.games = []
		self.tens = []

	def parse(self, name, tournament):
		""" Parse TNMT file, create player and game objects along the way """
		filename = "%s.tnmt" % name
		f = codecs.open(filename, 'r')
		length = file_len(filename)
		playerCount = 0

		# Go through entire file
		for i in range(length):
			line = f.readline()
			parts = line.split(":")
			
			# Create players from players listing			
			if parts[0] == "PLAYERS":
				playerCount = int(parts[1])
			
				for i in range(playerCount):
					length = length - 1
					line = f.readline()
					self.players.append(player.Player(line.strip()))
			
			# Group Stage / Round Robin matches
			elif parts[0] == "ROUND":
			
				for i in range(playerCount/2+1):
					
					line = f.readline()
					length = length - 1
					parts = line.split(":")
			
					if len(parts) == 4 or len(parts) == 5:
						p1 = getPlayer(self.players, parts[0])
						p2 = getPlayer(self.players, parts[1])
						# p1 = find(lambda person: person.name == parts[0], players)
						# p2 = find(lambda person: person.name == parts[1], players)
						g = game.Game(p1, p2, int(parts[2]), int(parts[3]), "ROBIN")
						
						# If either player scores 10 or more, match counts towards tens
						if int(parts[2]) >= 10 or int(parts[3]) >= 10:
			
							# if there's a overtime, disqualification, overwalk or previous groups -postfix, handle here
							if len(parts) == 5:
								self.tens.append(g)
								g.extra = parts[4]
							else:
								self.tens.append(g)

						# if there's a overtime, disqualification, overwalk or previous groups -postfix, handle here
						if len(parts) == 5:
							self.games.append(g)
							g.extra = parts[4]
						else:
							self.games.append(g)

			# Playoff matches
			elif parts[0].strip() == "PLAYOFFPAIR":
				length = length -1
				
				for i in range(7):
					line = f.readline()
					parts = line.split(":")
					

					if len(parts) == 4 or len(parts) == 5:
						p1 = getPlayer(self.players, parts[0])
						p2 = getPlayer(self.players, parts[1])
						g = game.Game(p1, p2, int(parts[2]), int(parts[3]), "PLAYOFF")
						
						# If either player scores 10 or more, match counts towards tens		
						if int(parts[2]) >= 10 or int(parts[3]) >= 10:
							

							# if there's a overtime, disqualification, overwalk or previous groups -postfix, handle here
							if len(parts) == 5:
								self.tens.append(g)
								g.extra = parts[4] 
							else:
								self.tens.append(g)

						# if there's a overtime, disqualification, overwalk or previous groups -postfix, handle here
						if len(parts) == 5:
							self.games.append(g)
							g.extra = parts[4] 
						else:
							self.games.append(g)
			else:
				pass

	
