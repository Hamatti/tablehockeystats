"""
Single table hockey game with two players, result, match type and tournament
"""

class Game(object):

  def __init__(self, p1, p2, hg, ag, match_type="n", extra=None, tournament=None):
  	self.player1 = p1
  	self.player2 = p2
  	self.hg = hg
  	self.ag = ag
  	self.match_type = match_type
  	self.extra = extra
  	self.tournament = tournament

  	self.processGame()

  # Update players' stats accordingly
  def processGame(self):
  	self.player1.addGame()
  	self.player2.addGame()
  	self.player1.addGoalsFor(self.hg)
  	self.player2.addGoalsFor(self.ag)
  	self.player1.addGoalsAgainst(self.ag)
  	self.player2.addGoalsAgainst(self.hg)
  	if self.hg > self.ag:
  		self.player1.addWin()
  		self.player2.addLoss()
  	elif self.hg < self.ag:
  		self.player1.addLoss()
  		self.player2.addWin()
  	else:
  		self.player1.addTie()
  		self.player2.addTie()
  	self.player1.countPoints()
  	self.player2.countPoints()

  # String represantation of object
  def __repr__(self):
  	return "%s - %s %d-%d" % (self.player1.name, self.player2.name, int(self.hg), int(self.ag))

