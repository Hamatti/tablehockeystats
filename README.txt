::::::::::::::::::::
:Table Hockey Stats:
::::::::::::::::::::

Author: Juha-Matti Santala, jumasan@utu.fi
Development Started: Spring 2012

Table Hockey Stats is a overall statistics software to control local series

It is compatible with Finnish Table Hockey Association's peli.jar scoresheet 
program and produces nice html for display.

