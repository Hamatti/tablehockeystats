# -*- coding: utf-8 -*-

import urllib
import xml.dom.minidom

wrURL = "http://www.ithf.info/stiga/ithf/ranking/getrank.asmx/GetRank?ID=%s"

# TODO:
#  print player's names correctly
#  now Juha Er\xc3\xa4laukko
#  should be Juha Erälaukko

class Player(object):

  def __init__(self, name, team=None, WRid=-1):
    self.name = name
    self.WRid = WRid
    self.team = team
    self.WRrank = None
    self.stats = {'games': 0, 'wins': 0, 'ties': 0, 'losses': 0, 'gf': 0, 'ga': 0, 'points': 0}
    if WRid == -1:
    	self.getID()
    if WRid != -1:
    	self.fetchRanking()
    


  # Assign team to player
  def addTeam(self, team):
	  self.team = team

  # Manually add World Ranking ID
  def addID(self, WRid):
  	self.WRid = WRid

  # Get World Ranking ID from ITHF text format ranking
  def getID(self):
    if self.WRid != -1:
      pass
    else:
      playerData = urllib.urlopen("http://ithf.info/stiga/ithf/ranking/ranking.txt")
      lineCount = 0
      for line in playerData:
        if lineCount == 0:
          lineCount += 1
        else:
          rank, id_player, name = line.split("\t")[:3]
          if name == self.name:
            self.WRid = id_player
    self.fetchRanking()
      		
  # Fetch ranking from ITHF World Ranking XML service
  def fetchRanking(self):
	  data = urllib.urlopen(wrURL%self.WRid)
	  rankXML = xml.dom.minidom.parse(data)
	  self.WRrank = rankXML.getElementsByTagName('string')[0].firstChild.data	

  # Basic "add stat" methods
  def addGame(self):
  	self.stats['games'] += 1

  def addWin(self):
  	self.stats['wins'] += 1 

  def addTie(self):
  	self.stats['ties'] += 1

  def addLoss(self):
  	self.stats['losses'] += 1

  def addGoalsFor(self, goals):
  	self.stats['gf'] += goals

  def addGoalsAgainst(self, goals):
  	self.stats['ga'] += goals

  def countPoints(self):
  	self.stats['points'] = 2 * self.stats['wins'] + self.stats['ties']

  # String representation of Player
  def __repr__(self):
  	return "%s (%d): WR: %s Team: %s Stats: %s" % (self.name, int(self.WRid), self.WRrank , self.team, self.stats )

 